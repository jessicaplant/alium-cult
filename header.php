<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="content-type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
    <meta name="description" content="<?php echo esc_attr( get_bloginfo( 'description' ) ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/jessica.css">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="fill-width white-bg make-it-sticky">
    <div class="flex-on flex-distribute fill-width col-on-tablet">
        <div class="menu-wrapper fill-width-on-tablet">
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'main_menu'
                ) );
            ?>
        </div>
        <div class="logo-wrapper flex-on flex-centre flex-centre-vertical pad-20-left-n-right fill-width-on-tablet">
            <img class="site-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="">
        </div>
    </div>
</div>