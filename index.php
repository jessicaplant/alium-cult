<?php get_header(); ?>

<?php while (have_posts()): the_post(); ?>
<div id="content" class="container main-container">
  <div class="fill-width pad-50-top-n-bottom centre-text">
      <h1 class="proximalight"><?php the_title(); ?></h1>
  </div>
    <div class="fill-width centre-text proximalight">
        <?php the_content(); ?>
    </div>
</div><!-- .container.main-container -->
<?php endwhile; ?>

<?php get_footer(); ?>
