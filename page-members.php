<?php get_header(); ?>

<?php
    $args = array(
        'post_type' => 'models',
        'posts_per_page' => -1
    );
    $models = get_posts($args);
?>

<div id="content" class="fill-width">
        <div class="flex-on flex-wrap fill-width">
		    <?php foreach ($models as $post): ?>
			    <?php setup_postdata($post); ?>
<!--                <a class="fill-width fill-height flex-third-width responsive-third-square" href="--><?php //the_permalink(); ?><!--">-->
                    <div class="position-relative flex-third-width fill-width fill-height responsive-third-square fill-background" style="background-image: url('<?php echo wp_get_attachment_image_src( get_field( 'profile_photo'), 'full' )[0]; ?>');">
                        <div class="show-on-hover see-thru-black-bg position-absolute fill-width fill-height flex-on flex-centre flex-centre-vertical">
                            <div class="flex-on flex-columns centre-text">
                                <h2 class="white-text"><?php the_title(); ?></h2>
                                <span class="white-text">view</span>
                            </div>

                        </div>



                    </div>
<!--                </a>-->









			    <?php wp_reset_postdata(); ?>
		    <?php endforeach; ?>
        </div>


</div><!-- .container.main-container -->

<?php get_footer(); ?>
